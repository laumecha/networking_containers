# Común
Para cada verisón de ha implementado un Dockerfile, el cual parte de una imagen ubuntu y ha servido unicamente para realizar pruebas.

Para construir la imagen se debe de lanzar el script:
```
sh setimage.sh
```
Para todas las versiones se proporcionan varios scripts:
 - setup[numversion].sh : crea los contenedores y realiza las configuraciones de red pedidaas.
 - delete[numversion].sh : borra las configuraciones y los contenedores creados.
 - check[numversion].sh : realiza una verificación del sistema mediante pings.

# Problema 1
En este problema, se han creado dos contenedores y, por cada uno de ellos, se ha creado un namespace. Además, se ha creado un par veth y se ha unido cada extremo al namespace correspondiente. Finalmente, después de asignar las IPs correspondientes, se han "levantado" los veths. En el script se encuentra el código comentado.


A continuación se detalla el esquema de este problema:

![Imagen](https://gitlab.com/laumecha/networking_containers/-/blob/master/problema1.png)

# Problema 2

En este problema, se han creado un contenedores y, para este, se ha creado un namespace. También hemos creado un bridge para actuar como conector. Además, se ha creado un par veth y se ha unido cada extremo al namespace correspondiente y al bridge. Después de asignar las IPs correspondientes, se han “levantado” los veths. Finalmente, indicamos que todas las conexiones del namespace del contenedor deben passar por el bridge. En el script se encuentra el código comentado.

A continuación se detalla el esquema de este problema:

![Imagen](https://gitlab.com/laumecha/networking_containers/-/blob/master/p2.png)

# Problema 3

En este problema, se han creado dos contenedores y, por cada uno de ellos, se ha creado un namespace. También hemos creado un bridge para actuar como conector. Además, se ha creado un dos pares veth y se ha unido cada extremo al namespace correspondiente y otro bridge. Después de asignar las IPs correspondientes, se han “levantado” los veths. Finalmente, indicamos que todas las conexiones de los namespaces de los contenedores deben passar por el bridge. En el script se encuentra el código comentado.

Además, para acceder a todos los hosts accesibles desde internet, se modificado la tabla NET mediante iptables para permitir esto.

A continuación se detalla el esquema de este problema:

![Imagen](https://gitlab.com/laumecha/networking_containers/-/blob/master/p3.png)
