#!/bin/bash
# -*- ENCODING: UTF-8 -*-
####Ejercicio tres###

#Crear contenedores 
echo "> Creando contenedores..."
docker run -i -t -d --net=none --name="con1" ejerciciocc
docker run -i -t -d --net=none --name="con2" ejerciciocc

#Crear namespaces visible para los contenedores
echo "> Creando namespaces..."
sudo mkdir -p /var/run/netns
sudo ln -sf /proc/$(docker inspect con1 -f '{{.State.Pid}}')/ns/net /var/run/netns/con1
sudo ln -sf /proc/$(docker inspect con2 -f '{{.State.Pid}}')/ns/net /var/run/netns/con2

#Crear un bridge
echo "> Creando bridge..."
sudo ip link add name bridge22 type bridge

#Crear dos veth 
echo "> Creando pares veth..."
sudo ip link add con1veth type veth peer name bri1veth
sudo ip link add con2veth type veth peer name bri2veth

#Añadir un extremo al namespace del contentedor
echo "> Asignando los extremos ..."
sudo ip link set netns con1 dev con1veth
sudo ip link set netns con2 dev con2veth

# Unir el otro extremo al bridge creado
sudo ip link set bri1veth master bridge22
sudo ip link set bri2veth master bridge22

#asignar una ip al bridge
echo "> Asignando ips ..."
sudo ip addr add 10.10.10.10/24 brd + dev bridge22

#asignar una ip a cada contenedor
sudo ip netns exec con1 ip addr add 10.10.10.11/24 dev con1veth
sudo ip netns exec con2 ip addr add 10.10.10.12/24 dev con2veth

#up
echo "> Up ..."
sudo ip netns exec con1 ip link set con1veth up
sudo ip netns exec con2 ip link set con2veth up

sudo ip netns exec con1 ip link set lo up
sudo ip netns exec con2 ip link set lo up

sudo ip link set bridge22 up

sudo ip link set bri1veth up
sudo ip link set bri2veth up 

#??
echo "> setting default..."
sudo ip netns exec con1 ip route add default via 10.10.10.10
sudo ip netns exec con2 ip route add default via 10.10.10.10
#ip -all netns exec ip route add default via 10.10.10.10

echo "> setting iptables..."
sudo iptables -F FORWARD
sudo iptables -P FORWARD ACCEPT

sudo sysctl -w net.ipv4.ip_forward=1
sudo iptables -t nat -A POSTROUTING -s 10.10.10.0/24 -j MASQUERADE

echo "> DONE"
