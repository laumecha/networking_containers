#!/bin/bash
# -*- ENCODING: UTF-8 -*-
####Ejercicio uno###
docker build --tag ejerciciocc .

#Crear contenedores 
echo "> Creando contenedores..."
docker run -i -t -d --net=none --name="con1" ejerciciocc
docker run -i -t -d --net=none --name="con2" ejerciciocc

#Crear namespaces visible para los contenedores
echo "> Creando namespaces..."
sudo ln -sf /proc/$(docker inspect con1 -f '{{.State.Pid}}')/ns/net /var/run/netns/con1
sudo ln -sf /proc/$(docker inspect con2 -f '{{.State.Pid}}')/ns/net /var/run/netns/con2

#Crear un par veth entre los dos contenedores
echo "> Creando el par veth..."
sudo ip link add con1veth type veth peer name con2veth

#asignar un par a cada contenedor
echo "> Asignando pares..."
sudo ip link set netns con1 dev con1veth
sudo ip link set netns con2 dev con2veth

#asignar ip
echo "> Asignando ips..."
sudo ip netns exec con1 ip addr add 10.10.10.1/24 dev con1veth
sudo ip netns exec con2 ip addr add 10.10.10.2/24 dev con2veth
#up
echo "> Up..."
sudo ip netns exec con1 ip link set con1veth up && sudo ip netns exec con2 ip link set con2veth up 
echo "> OK"