#!/bin/bash
# -*- ENCODING: UTF-8 -*-
####Ejercicio dos###
#Crear contenedores 
echo "> Creando contenedor..."
docker run -i -t -d --net=none --name="con1" ubuntu 

#Crear namespaces visible para los contenedores
sudo mkdir -p /var/run/netns
echo "> Creando namespaces..."
sudo ln -sf /proc/$(docker inspect con1 -f '{{.State.Pid}}')/ns/net /var/run/netns/con1

#Crear un bridge
echo "> Creando bridge..."
sudo ip link add name bridge22 type bridge

#Crear un veth
echo "> Creando el par veth..."
sudo ip link add con1veth type veth peer name con2veth

#Añadir un extremo al namespace del contentedor
echo "> Asignando pares..."
sudo ip link set netns con1 dev con1veth

# Unir el otro extremo al bridge creado
sudo ip link set con2veth master bridge22

#asignar una ip al bridge
echo "> Asignando ips..."
sudo ip addr add 10.10.10.2/24 brd + dev bridge22

#asignar una ip al con1
sudo ip netns exec con1 ip addr add 10.10.10.1/24 dev con1veth

#up
echo "> Up..."
sudo ip netns exec con1 ip link set con1veth up
sudo ip netns exec con1 ip link set lo up
sudo ip link set con2veth up && sudo ip link set bridge22 up

sudo ip netns exec con1 ip route add default via 10.10.10.2
echo "> OK"